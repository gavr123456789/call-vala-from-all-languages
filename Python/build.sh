valac -H object.h -C --vapi=object.vapi --library=libobject object.vala;
valac --enable-experimental -X -fPIC -X -shared --library=libobject --gir=ValaObject-0.1.gir -o libobject.so object.vala;
g-ir-compiler --shared-library=libobject.so --output=ValaObject-0.1.typelib ValaObject-0.1.gir;