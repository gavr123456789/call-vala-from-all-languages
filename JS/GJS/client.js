const T = imports.gi.TestShared; 
 
print ("square:\t" + T.square(42));
print ("repeat:\t" + T.repeat ("Hi", 4));
 
print("array:\t" + T.anarray())
print("map:\t" + T.amap()["Hello"])
print("list:\t" + T.alist())

let [a, b] = T.multiple_return_values ();
print("multi.:\t" + [a, b]);
 
T.dump_dict ({name: "foo", val: 42});

var obj = new T.MyClass ();

obj.connect("my_signal", function(sig, str) {
    print ("Signal callback: " + str);}
);
obj.emit ("Hi!");


print("Property: " + obj.avalue)