namespace TestShared {

    public int square (int a) {
        return (a * a);
    }

    public void repeat (string s, int n,
                        [CCode (array_length = false)]
                        out string[] r) {
        r = new string[n];
        for (int i = 0; i < n; i++) {
            r[i] = s;
        }
    }

    [CCode (array_length = false)]
    public string[] anarray () {
        return new string[] { "a", "b" };
    }

    public HashTable<string, string> amap () {
        var r = new HashTable<string, string>(null, null);
        r.insert ("Hello", "World");
        return r;
    }

    public List<string> alist () {
        var r = new List<string>();
        r.append ("W");
        r.append ("o");
        r.append ("r");
        return r;
    }

    public void multiple_return_values (out int a, out int b) {
        a = 1;
        b = 2;
    }

    public void dump_dict (HashTable<string, Value?> dict) {
        foreach (var k in dict.get_keys ()) {
            print ("  %s: %s\n", k, dict.lookup (k).strdup_contents ());
        }
    }

    public class MyClass : Object {
        public string anonWorkinValue = "no property";
        public string avalue { get; set; default = "this is a property"; }
        public signal void my_signal (string s);
        public void emit (string s) {
            this.my_signal (s);
        }
    }
}