valac --ccode --header=testShared.h \
   --gir=TestShared-0.1.gir \
   --library=TestShared-0.1 \
   --pkg=gio-2.0 testShared.vala

gcc -fPIC -shared -o libTestShared.so \
   $(pkg-config --libs --cflags gobject-2.0) \
   testShared.c

g-ir-compiler --shared-library=libTestShared.so \
   --output=TestShared-0.1.typelib TestShared-0.1.gir

GI_TYPELIB_PATH=. LD_LIBRARY_PATH=. \
   gjs --command="const TestShared = imports.gi.TestShared; \
   print('Square of 42: ' + TestShared.square (42));"