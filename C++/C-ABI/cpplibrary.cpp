# include <iostream>
# include <cstring>
# include <string>
using namespace std;

extern "C" const char * cpp_test_function (const char * ch) {
    std::string str (ch);
    cout << "This is string from vala code: \"" << str <<"\"\n";
    str = str + u8" and from C++ now"; 
    char * cstr = new char [str.length()+1];
    std::strcpy (cstr, str.c_str());
    return cstr;
}