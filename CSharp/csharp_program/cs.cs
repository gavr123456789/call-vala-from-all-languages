using System;
using System.Runtime.InteropServices;
namespace ValaExample
{
    class Program
    {
        [DllImport("valalib.so", CallingConvention = CallingConvention.Cdecl)]
        public static extern double Add(double a, double b);
        static void Main()
        {
            var from_vala = Add(41.5, 0.5);
            Console.WriteLine($"Result from ValaLib: 41.5 + 0.5 = {from_vala}"); 
        }
    }
}