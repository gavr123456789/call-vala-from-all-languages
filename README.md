# Call Vala From All Languages

These are examples of how to call Vala class natively from other programming languages. You can use Vala in the narrow necks of your projects.  
  
# Usage  
All folders contain 3 scripts and necessary files. For example:  
```bash
$ sh build.sh  
$ sh run.sh test.py
$ sh clear.sh
```